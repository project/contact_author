<?php

namespace Drupal\contact_author\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ContactAuthorBlock' block.
 *
 * @Block(
 *  id = "contact_author",
 *  admin_label = @Translation("Contact author"),
 * )
 */
class ContactAuthorBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * RedirectDestination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The current user.
   *
   * @var Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->routeMatch = $container->get('current_route_match');
    $instance->redirectDestination = $container->get('redirect.destination');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $node = $this->routeMatch->getParameter('node');

    if (!$node instanceof NodeInterface) {
      return $build;
    }

    $route_parameters = ['user' => $node->getOwnerId()];
    $options = ['query' => $this->redirectDestination->getAsArray()];
    $url = Url::fromRoute('entity.user.contact_form', $route_parameters, $options);

    $build['#theme'] = 'contact_author';
    $build['#url'] = $url->toString();
    $build['#label'] = $this->getConfiguration()['label'];

    // If we use Oliviero theme and attach the 'core/drupal.dialog.ajax' library
    // for authenticated users, then error messages displayed in a browser's
    // console (for Bartik theme its wok without problems). Therefore, we attach
    // the library only for anonymous users.
    if ($this->currentUser->isAnonymous()) {
      $build['#attached']['library'] = ['core/drupal.dialog.ajax'];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access user contact forms');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
